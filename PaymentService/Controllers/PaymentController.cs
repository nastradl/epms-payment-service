﻿using Dapper;
using Oracle.ManagedDataAccess.Client;
using PaymentService.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace PaymentService.Controllers
{
    public class PaymentController : ApiController
    {
        private readonly string _conn = ConfigurationManager.AppSettings["ConnString"];

        [Route("api/Payment/GetPayment")]
        public Payment GetPayment(string refid, string aid)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                string query = @"SELECT AID, REFID, FIRSTNAME, LASTNAME, DOB, STATEOFBIRTH, SEX, PROCESSINGCOUNTRY, C.COUNTRYNAME AS PROCESSINGCOUNTRYNAME, PROCESSINGSTATE, S.STATENAME AS PROCESSINGSTATENAME, 
                                PROCESSINGOFFICE, O.OFFICENAME AS PROCESSINGOFFICENAME, MYDATETIME, PAYMENTDATE,
                                PASSPORTTYPE, EXPIRYDATE, FORMNO, ACTIVATIONNO, APPLICATIONSTATUS, ENROLMENTDATE, POSTINGSTATE, AMOUNT_NAIRA, AMOUNT_DOLLAR, PASSPORTSIZE,
                                INTERVENTIONOFFICE, INTERVENTIONSTATUS, PROCESSINGCOUNTRY
                                FROM EPMS_PMT_DAT.payment_ref_table P LEFT JOIN EPMS_PMT_DAT.PROCESSING_COUNTRY C ON C.COUNTRYID = P.PROCESSINGCOUNTRY
                                LEFT JOIN EPMS_PMT_DAT.PROCESSING_OFFICE O ON O.OFFICEID = P.PROCESSINGOFFICE
                                LEFT JOIN EPMS_PMT_DAT.PROCESSING_STATE S ON S.STATEID = P.PROCESSINGSTATE  WHERE refid = :refid and aid = :aid";
                var payment = dbConn.Query<Payment>(query, new { aid, refid }).FirstOrDefault();

                if (payment == null)
                    return null;

                // convert dates
                payment.Dob = string.IsNullOrEmpty(payment.Dob) ? "" : DateTime.Parse(payment.Dob).ToString("dd/MM/yyyy hh:mm:ss tt");
                payment.Expirydate = string.IsNullOrEmpty(payment.Expirydate) ? "" : DateTime.Parse(payment.Expirydate).ToString("dd/MM/yyyy hh:mm:ss tt");
                payment.Enrolmentdate = string.IsNullOrEmpty(payment.Enrolmentdate) ? "" : DateTime.Parse(payment.Enrolmentdate).ToString("dd/MM/yyyy hh:mm:ss tt");
                payment.Paymentdate = string.IsNullOrEmpty(payment.Paymentdate) ? "" : DateTime.Parse(payment.Paymentdate).ToString("dd/MM/yyyy hh:mm:ss tt");

                return payment;
            }
        }

        [Route("api/Payment/GetProcessingCountries")]
        public List<ProcessingCountry> GetProcessingCountries()
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<ProcessingCountry>("SELECT COUNTRYID, COUNTRYNAME, COUNTRYCODE FROM EPMS_PMT_DAT.processing_country").ToList();
            }
        }

        [Route("api/Payment/GetProcessingOffices")]
        public List<ProcessingOffice> GetProcessingOffices()
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<ProcessingOffice>("SELECT OFFICEID, OFFICENAME FROM EPMS_PMT_DAT.processing_office").ToList();
            }
        }

        [Route("api/Payment/GetProcessingStates")]
        public List<ProcessingState> GetProcessingStates()
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();
                return dbConn.Query<ProcessingState>("SELECT STATEID, STATENAME FROM EPMS_PMT_DAT.processing_state").ToList();
            }
        }

        [Route("api/Payment/EditPaymentAdmin")]
        public IHttpActionResult EditPaymentAdmin(Payment payment)
        {
            string query = @"UPDATE EPMS_PMT_DAT.payment_ref_table SET firstname = :firstname, lastname = :lastname, dob = :dob, sex = :sex,
                            expirydate = :expirydate, processingstate = :processingstate, processingoffice = :processingoffice WHERE aid = :aid and refid = :refid";

            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var affectedRows = dbConn.Execute(query, new
                {
                    aid = payment.Aid,
                    refid = payment.RefId,
                    firstname = payment.Firstname,
                    lastname = payment.Lastname,
                    dob = DateTime.Parse(payment.Dob),
                    sex = payment.Sex,
                    expirydate = DateTime.Parse(payment.Expirydate),
                    processingstate = payment.Processingstate,
                    processingoffice = payment.Processingoffice
                });

                if (affectedRows == 0)
                    return BadRequest();

                return Ok();
            }
        }

        [Route("api/Payment/EditPayment")]
        public IHttpActionResult EditPayment(Payment payment)
        {
            string query = @"UPDATE EPMS_PMT_DAT.payment_ref_table SET firstname = :firstname, lastname = :lastname, dob = :dob, sex = :sex
                            WHERE aid = :aid and refid = :refid";

            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                var affectedRows = dbConn.Execute(query, new
                {
                    aid = payment.Aid,
                    refid = payment.RefId,
                    firstname = payment.Firstname,
                    lastname = payment.Lastname,
                    dob = DateTime.Parse(payment.Dob),
                    sex = payment.Sex,
                    expirydate = DateTime.Parse(payment.Expirydate),
                    processingstate = payment.Processingstate,
                    processingoffice = payment.Processingoffice
                });

                if (affectedRows == 0)
                    return BadRequest();

                return Ok();
            }
        }

        [HttpGet]
        [Route("api/Payment/ResetPayment")]
        public IHttpActionResult ResetPayment(string aid, string refid)
        {
            using (var dbConn = new OracleConnection(_conn))
            {
                dbConn.Open();

                // First confirm that the application status is not EM5000
                string query = "select applicationstatus from EPMS_PMT_DAT.payment_ref_table where aid = :aid and refid = :refid";

                var status = dbConn.Query<string>(query, new
                {
                    aid,
                    refid
                }).SingleOrDefault();

                if (status == null)
                    return Content(HttpStatusCode.BadRequest, 0);

                if (status == "EM5000")
                    return Content(HttpStatusCode.BadRequest, 2);

                query = @"update EPMS_PMT_DAT.payment_ref_table 
                            set applicationstatus = 'EM0500',
                            enrolmentdate = null, 
                            postingstate = 'PS0500',
                            formno = null 
                            where aid = :aid and refid = :refid and applicationstatus != 'EM5000'";

                var affectedRows = dbConn.Execute(query, new
                {
                    aid,
                    refid
                });

                if (affectedRows == 0)
                    return BadRequest();

                return Ok();
            }
        }

        [Route("api/SaveFingerprint")]
        public int SaveFingerprint(byte[] fingerprint)
        {
            try
            {
                System.IO.File.WriteAllBytes(@"C:\Forensics\Fingerprints\fingerprint.wsq", fingerprint);
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}